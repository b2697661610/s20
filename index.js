
let number = Number(prompt("Give me a number:"));
console.log("The number you provided is " + number);

for(let count = number; count >= 0; count--){
	
	if(count <= 50){
		console.log("The current value is at " + count + ". Terminating the loop");
		break;
	} else if(count % 10 === 0) { 
		console.log("The number is divisible by 10. Skipping the number!");

		continue;		
	} else if(count % 5 === 0) { 
		console.log(count);
	}
}


// 2.

let string = 'supercalifragilisticexpialidocious';

let filtered_string = '';

for(let index = 0; index < string.length; index++){
	console.log(string[index]);

	if(
		string[index].toLowerCase() == 'a' ||
		string[index].toLowerCase() == 'e' ||
		string[index].toLowerCase() == 'i' ||
		string[index].toLowerCase() == 'o' ||
		string[index].toLowerCase() == 'u' 
	){
		// If the current letter is a vowel then skip that.
		continue;
	} else {
		filtered_string += string[index];
	}
}

console.log(filtered_string);
